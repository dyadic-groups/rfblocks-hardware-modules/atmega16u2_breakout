# An Atmega32U2 Controller Board

## Features

- Light weight design with minimal components.
- Serial control via USB/RS232 from host computer.
- SPI control of devices in addition to 16 general IO lines.

## Documentation

Full documentation for the controller board is available at
[RF Blocks](https://rfblocks.org/boards/Atmega32U2-Board.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
