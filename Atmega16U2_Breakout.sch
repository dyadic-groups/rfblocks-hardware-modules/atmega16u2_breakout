EESchema Schematic File Version 4
LIBS:Atmega16U2_Breakout-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Atmega16U2 Breakout"
Date "2019-02-17"
Rev "2.2"
Comp "Dyadic Pty. Ltd."
Comment1 "2020"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L device:C_Small C1
U 1 1 562F4AB3
P 3825 3750
F 0 "C1" V 3900 3600 50  0000 L CNN
F 1 "15p" V 3750 3575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3825 3750 60  0001 C CNN
F 3 "" H 3825 3750 60  0000 C CNN
F 4 "Kemet " H 3825 3750 50  0001 C CNN "MFN"
F 5 "C0603C150J5GACTU" H 3825 3750 50  0001 C CNN "MFP"
F 6 "399-1051-1-ND" H 3825 3750 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 3825 3750 50  0001 C CNN "S1PL"
F 8 "80-C0603C150J5G" H 3825 3750 50  0001 C CNN "S2PN"
F 9 "Mouser" H 3825 3750 50  0001 C CNN "S2PL"
F 10 "CAP CER 15PF 50V C0G/NP0 0603" H 3825 3750 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric)" H 3825 3750 50  0001 C CNN "Package ID"
	1    3825 3750
	0    1    1    0   
$EndComp
$Comp
L project:CONN_01X16 P1
U 1 1 562F4D09
P 2100 1150
F 0 "P1" H 2100 2000 50  0000 C CNN
F 1 "~" V 2200 1150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 2100 1150 60  0001 C CNN
F 3 "" H 2100 1150 60  0000 C CNN
F 4 "BOOMELE" H 2100 1150 50  0001 C CNN "MFN"
F 5 "C50981" H 2100 1150 50  0001 C CNN "MFP"
F 6 "C50981" H 2100 1150 50  0001 C CNN "S1PN"
F 7 "LCSC" H 2100 1150 50  0001 C CNN "S1PL"
F 8 "Pin Header Male Pin 0.100\"(2.54mm) 16 pin" H 2100 1150 50  0001 C CNN "Description"
F 9 "https://lcsc.com/product-detail/Pin-Header-Female-Header_BOOMELE-Boom-Precision-Elec-C50981_C50981.html" H 2100 1150 50  0001 C CNN "kicost:LCSC:link"
F 10 "1:$0.092897; 5:0.092897; 50:$0.06793; 150:$0.063344" H 2100 1150 50  0001 C CNN "kicost:LCSC:pricing"
	1    2100 1150
	0    -1   -1   0   
$EndComp
$Comp
L project:CONN_01X16 P2
U 1 1 562F4D3C
P 2100 2350
F 0 "P2" H 2100 3200 50  0000 C CNN
F 1 "~" V 2200 2350 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 2100 2350 60  0001 C CNN
F 3 "" H 2100 2350 60  0000 C CNN
F 4 "BOOMELE" H 2100 2350 50  0001 C CNN "MFN"
F 5 "C50981" H 2100 2350 50  0001 C CNN "MFP"
F 6 "C50981" H 2100 2350 50  0001 C CNN "S1PN"
F 7 "LCSC" H 2100 2350 50  0001 C CNN "S1PL"
F 8 "Pin Header Male Pin 0.100\"(2.54mm) 16 pin" H 2100 2350 50  0001 C CNN "Description"
F 9 "1:$0.092897; 5:0.092897; 50:$0.06793; 150:$0.063344" H 2100 2350 50  0001 C CNN "kicost:LCSC:pricing"
F 10 "https://lcsc.com/product-detail/Pin-Header-Female-Header_BOOMELE-Boom-Precision-Elec-C50981_C50981.html" H 2100 2350 50  0001 C CNN "kicost:LCSC:link"
	1    2100 2350
	0    -1   -1   0   
$EndComp
$Comp
L project:CONN_02X03 P3
U 1 1 562F4D66
P 7950 2650
F 0 "P3" H 7950 2850 50  0000 C CNN
F 1 "ICSP" H 7950 2450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7950 1450 60  0001 C CNN
F 3 "" H 7950 1450 60  0000 C CNN
F 4 "Mintron" H 7950 2650 50  0001 C CNN "MFN"
F 5 "MTP125-203R1" H 7950 2650 50  0001 C CNN "MFP"
F 6 "C358705" H 7950 2650 50  0001 C CNN "S1PN"
F 7 "LCSC" H 7950 2650 50  0001 C CNN "S1PL"
F 8 "Pin Header Male Pin 0.100\"(2.54mm) 6 pin, 2 row" H 7950 2650 50  0001 C CNN "Description"
F 9 "Through Hole,P=2.54mm" H 7950 2650 50  0001 C CNN "Package ID"
F 10 "1:$0.0395; 20:$0.0395; 200:$0.028605" H 7950 2650 50  0001 C CNN "kicost:LCSC:pricing"
F 11 "https://lcsc.com/product-detail/Pin-Header-Female-Header_MINTRON-MTP125-203R1_C358705.html" H 7950 2650 50  0001 C CNN "kicost:LCSC:link"
	1    7950 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 562FDABA
P 3450 4050
F 0 "#PWR01" H 3450 3800 50  0001 C CNN
F 1 "GND" H 3450 3900 50  0000 C CNN
F 2 "" H 3450 4050 60  0000 C CNN
F 3 "" H 3450 4050 60  0000 C CNN
	1    3450 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 562FDCAB
P 6000 6050
F 0 "#PWR02" H 6000 5800 50  0001 C CNN
F 1 "GND" H 6000 5900 50  0000 C CNN
F 2 "" H 6000 6050 60  0000 C CNN
F 3 "" H 6000 6050 60  0000 C CNN
	1    6000 6050
	1    0    0    -1  
$EndComp
$Comp
L device:C C3
U 1 1 562FDD85
P 4600 5650
F 0 "C3" H 4625 5750 50  0000 L CNN
F 1 "1u" H 4625 5550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4638 5500 30  0001 C CNN
F 3 "" H 4600 5650 60  0000 C CNN
F 4 "Kemet" H 4600 5650 50  0001 C CNN "MFN"
F 5 "C0805C105K4RACTU " H 4600 5650 50  0001 C CNN "MFP"
F 6 "399-1284-1-ND" H 4600 5650 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 4600 5650 50  0001 C CNN "S1PL"
F 8 "C0805C105K4RACTU" H 4600 5650 50  0001 C CNN "S2PN"
F 9 "Arrow" H 4600 5650 50  0001 C CNN "S2PL"
F 10 "CAP CER 1UF 16V X7R 0805 " H 4600 5650 50  0001 C CNN "Description"
F 11 "0805 (2012 Metric)" H 4600 5650 50  0001 C CNN "Package ID"
	1    4600 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 562FDE36
P 4600 6050
F 0 "#PWR03" H 4600 5800 50  0001 C CNN
F 1 "GND" H 4600 5900 50  0000 C CNN
F 2 "" H 4600 6050 60  0000 C CNN
F 3 "" H 4600 6050 60  0000 C CNN
	1    4600 6050
	1    0    0    -1  
$EndComp
Text GLabel 8550 3300 2    60   Input ~ 0
PB0
Text GLabel 8550 3400 2    60   Input ~ 0
PB1
Text GLabel 8550 3500 2    60   Input ~ 0
PB2
Text GLabel 8550 3600 2    60   Input ~ 0
PB3
Text GLabel 8550 3700 2    60   Input ~ 0
PB4
Text GLabel 8550 3800 2    60   Input ~ 0
PB5
Text GLabel 8550 3900 2    60   Input ~ 0
PB6
Text GLabel 8550 4000 2    60   Input ~ 0
PB7
Text GLabel 4800 3700 0    60   Input ~ 0
~RESET
Text GLabel 7250 2550 0    60   Input ~ 0
~RESET
Text GLabel 7150 2750 0    60   Input ~ 0
VCC
$Comp
L power:GND #PWR04
U 1 1 562FEC80
P 8650 2850
F 0 "#PWR04" H 8650 2600 50  0001 C CNN
F 1 "GND" H 8650 2700 50  0000 C CNN
F 2 "" H 8650 2850 60  0000 C CNN
F 3 "" H 8650 2850 60  0000 C CNN
	1    8650 2850
	1    0    0    -1  
$EndComp
Text GLabel 8550 4200 2    60   Input ~ 0
PD0
Text GLabel 8550 4300 2    60   Input ~ 0
PD1
Text GLabel 8550 4400 2    60   Input ~ 0
PD2
Text GLabel 8550 4500 2    60   Input ~ 0
PD3
Text GLabel 8550 4600 2    60   Input ~ 0
PD4
Text GLabel 8550 4700 2    60   Input ~ 0
PD5
Text GLabel 8550 4800 2    60   Input ~ 0
PD6
Text GLabel 8550 4900 2    60   Input ~ 0
PD7
Text GLabel 8550 5100 2    60   Input ~ 0
PC2
Text GLabel 8550 5200 2    60   Input ~ 0
PC4
Text GLabel 8550 5300 2    60   Input ~ 0
PC5
Text GLabel 8550 5400 2    60   Input ~ 0
PC6
Text GLabel 8550 5500 2    60   Input ~ 0
PC7
$Comp
L device:R R1
U 1 1 562FFA33
P 4100 4800
F 0 "R1" V 4050 5000 50  0000 C CNN
F 1 "22" V 4100 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4030 4800 30  0001 C CNN
F 3 "" H 4100 4800 30  0000 C CNN
F 4 "Panasonic" H 4100 4800 50  0001 C CNN "MFN"
F 5 "ERJ-3EKF22R0V" H 4100 4800 50  0001 C CNN "MFP"
F 6 "P22.0HCT-ND" H 4100 4800 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 4100 4800 50  0001 C CNN "S1PL"
F 8 "667-ERJ-3EKF22R0V" H 4100 4800 50  0001 C CNN "S2PN"
F 9 "Mouser" H 4100 4800 50  0001 C CNN "S2PL"
F 10 "RES SMD 22 OHM 1% 1/10W 0603" H 4100 4800 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric) " H 4100 4800 50  0001 C CNN "Package ID"
	1    4100 4800
	0    1    1    0   
$EndComp
$Comp
L device:L_Small L1
U 1 1 562FFB9C
P 3175 5900
F 0 "L1" H 3205 5940 50  0000 L CNN
F 1 "BLM21" H 3205 5860 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3175 5900 60  0001 C CNN
F 3 "" H 3175 5900 60  0000 C CNN
F 4 "Murata" H 3175 5900 50  0001 C CNN "MFN"
F 5 "BLM21PG221SN1D " H 3175 5900 50  0001 C CNN "MFP"
F 6 "BLM21PG221SN1D " H 3175 5900 50  0001 C CNN "S1PN"
F 7 "Arrow" H 3175 5900 50  0001 C CNN "S1PL"
F 8 "490-1054-1-ND" H 3175 5900 50  0001 C CNN "S2PN"
F 9 "Digi-Key" H 3175 5900 50  0001 C CNN "S2PL"
F 10 "FERRITE BEAD 220 OHM 0805 1LN " H 3175 5900 50  0001 C CNN "Description"
F 11 "0805 (2012 Metric) " H 3175 5900 50  0001 C CNN "Package ID"
	1    3175 5900
	1    0    0    -1  
$EndComp
NoConn ~ 2450 5000
$Comp
L power:GND #PWR05
U 1 1 56300EC0
P 3175 6050
F 0 "#PWR05" H 3175 5800 50  0001 C CNN
F 1 "GND" H 3175 5900 50  0000 C CNN
F 2 "" H 3175 6050 60  0000 C CNN
F 3 "" H 3175 6050 60  0000 C CNN
	1    3175 6050
	1    0    0    -1  
$EndComp
Text GLabel 3200 4400 0    60   Input ~ 0
USBVCC
Text GLabel 2250 2850 3    60   Input ~ 0
PD0
Text GLabel 2150 2850 3    60   Input ~ 0
PD1
Text GLabel 2050 2850 3    60   Input ~ 0
PD2
Text GLabel 1950 2850 3    60   Input ~ 0
PD3
Text GLabel 1850 2850 3    60   Input ~ 0
PD4
Text GLabel 1750 2850 3    60   Input ~ 0
PD5
Text GLabel 1650 2850 3    60   Input ~ 0
PD6
Text GLabel 1550 2850 3    60   Input ~ 0
PD7
$Comp
L device:C C4
U 1 1 56301A7B
P 4650 3050
F 0 "C4" H 4675 3150 50  0000 L CNN
F 1 "100n" H 4675 2950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4688 2900 30  0001 C CNN
F 3 "" H 4650 3050 60  0000 C CNN
F 4 "Samsung Electrro-mechanics" H 4650 3050 50  0001 C CNN "MFN"
F 5 "CL10B104KB8NNNC" H 4650 3050 50  0001 C CNN "MFP"
F 6 "CL10B104KB8NNNC" H 4650 3050 50  0001 C CNN "S1PN"
F 7 "Arrow" H 4650 3050 50  0001 C CNN "S1PL"
F 8 "1276-1000-1-ND" H 4650 3050 50  0001 C CNN "S2PN"
F 9 "Digi-Key" H 4650 3050 50  0001 C CNN "S2PL"
F 10 "CAP CER 0.1UF 50V X7R 0603" H 4650 3050 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric)" H 4650 3050 50  0001 C CNN "Package ID"
	1    4650 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 56301AF9
P 4650 3300
F 0 "#PWR06" H 4650 3050 50  0001 C CNN
F 1 "GND" H 4650 3150 50  0000 C CNN
F 2 "" H 4650 3300 60  0000 C CNN
F 3 "" H 4650 3300 60  0000 C CNN
	1    4650 3300
	1    0    0    -1  
$EndComp
Text GLabel 1450 2850 3    60   Input ~ 0
PB0
Text GLabel 1450 1650 3    60   Input ~ 0
PB1
Text GLabel 1550 1650 3    60   Input ~ 0
PB2
Text GLabel 1650 1650 3    60   Input ~ 0
PB3
Text GLabel 1750 1650 3    60   Input ~ 0
PB4
Text GLabel 1850 1650 3    60   Input ~ 0
PB5
Text GLabel 1950 1650 3    60   Input ~ 0
PB6
Text GLabel 2050 1650 3    60   Input ~ 0
PB7
Text GLabel 2750 2850 3    60   Input ~ 0
VCC
Text GLabel 2550 2850 3    60   Input ~ 0
USBVCC
Text GLabel 2450 1650 3    60   Input ~ 0
PC4
Text GLabel 2350 1650 3    60   Input ~ 0
PC5
Text GLabel 2250 1650 3    60   Input ~ 0
PC6
Text GLabel 2150 1650 3    60   Input ~ 0
PC7
Text GLabel 2350 2850 3    60   Input ~ 0
PC2
$Comp
L power:GND #PWR07
U 1 1 5630480E
P 2850 1450
F 0 "#PWR07" H 2850 1200 50  0001 C CNN
F 1 "GND" H 2850 1300 50  0000 C CNN
F 2 "" H 2850 1450 60  0000 C CNN
F 3 "" H 2850 1450 60  0000 C CNN
	1    2850 1450
	1    0    0    -1  
$EndComp
Text GLabel 1350 1550 3    60   Input ~ 0
~RESET
NoConn ~ 2450 2550
NoConn ~ 2650 2550
NoConn ~ 2550 1350
NoConn ~ 2650 1350
$Comp
L power:PWR_FLAG #FLG08
U 1 1 5630599B
P 4900 2800
F 0 "#FLG08" H 4900 2895 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 2980 50  0000 C CNN
F 2 "" H 4900 2800 60  0000 C CNN
F 3 "" H 4900 2800 60  0000 C CNN
	1    4900 2800
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG09
U 1 1 56305A4D
P 3725 4700
F 0 "#FLG09" H 3725 4795 50  0001 C CNN
F 1 "PWR_FLAG" H 3725 4880 50  0000 C CNN
F 2 "" H 3725 4700 60  0000 C CNN
F 3 "" H 3725 4700 60  0000 C CNN
	1    3725 4700
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG010
U 1 1 56305BEA
P 2550 5700
F 0 "#FLG010" H 2550 5795 50  0001 C CNN
F 1 "PWR_FLAG" H 2550 5880 50  0000 C CNN
F 2 "" H 2550 5700 60  0000 C CNN
F 3 "" H 2550 5700 60  0000 C CNN
	1    2550 5700
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG011
U 1 1 5630607F
P 3025 2700
F 0 "#FLG011" H 3025 2795 50  0001 C CNN
F 1 "PWR_FLAG" H 3025 2880 50  0000 C CNN
F 2 "" H 3025 2700 60  0000 C CNN
F 3 "" H 3025 2700 60  0000 C CNN
	1    3025 2700
	-1   0    0    1   
$EndComp
NoConn ~ 2750 1350
$Comp
L project:AB2_JMP_02-1 J1
U 1 1 565A414B
P 7675 1850
F 0 "J1" V 7825 1800 60  0000 C CNN
F 1 "SP" V 7675 1950 60  0000 C CNN
F 2 "ab2_jumper:AB2_SJMP_02-1" H 7675 1850 60  0001 C CNN
F 3 "" H 7675 1850 60  0000 C CNN
F 4 "1" V 7675 1850 50  0001 C CNN "DNP"
	1    7675 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 4000 4350 4000
Wire Wire Line
	4350 4000 4350 4150
Wire Wire Line
	4350 4150 4125 4150
Wire Wire Line
	4125 4050 4125 4100
Connection ~ 4125 4150
Wire Wire Line
	5100 3900 4350 3900
Wire Wire Line
	4350 3900 4350 3750
Wire Wire Line
	4350 3750 4125 3750
Wire Wire Line
	4125 3750 4125 3800
Connection ~ 4125 3750
Wire Wire Line
	3725 3750 3600 3750
Wire Wire Line
	3600 3750 3600 3950
Wire Wire Line
	3600 4150 3725 4150
Wire Wire Line
	3600 3950 3450 3950
Wire Wire Line
	3450 3950 3450 4050
Connection ~ 3600 3950
Wire Wire Line
	5100 5000 4900 5000
Wire Wire Line
	4900 5000 4900 5950
Wire Wire Line
	4900 5950 6000 5950
Wire Wire Line
	6200 5950 6200 5800
Wire Wire Line
	6000 5800 6000 5950
Connection ~ 6000 5950
Wire Wire Line
	5100 4600 4600 4600
Wire Wire Line
	4600 4400 4600 4600
Wire Wire Line
	4600 5800 4600 6050
Wire Wire Line
	5100 3800 4900 3800
Wire Wire Line
	4900 3800 4900 2800
Wire Wire Line
	4650 2800 4900 2800
Wire Wire Line
	6200 2600 6200 2800
Wire Wire Line
	7300 3300 8550 3300
Wire Wire Line
	7300 3400 7500 3400
Wire Wire Line
	7300 3500 8400 3500
Wire Wire Line
	7300 3600 8300 3600
Wire Wire Line
	7300 3700 8550 3700
Wire Wire Line
	7300 3800 8550 3800
Wire Wire Line
	7300 3900 8550 3900
Wire Wire Line
	7300 4000 8550 4000
Wire Wire Line
	7500 2650 7700 2650
Wire Wire Line
	7250 2550 7700 2550
Wire Wire Line
	5100 3700 4800 3700
Wire Wire Line
	8200 2650 8400 2650
Wire Wire Line
	7700 2750 7150 2750
Wire Wire Line
	8200 2750 8650 2750
Wire Wire Line
	8650 2750 8650 2850
Wire Wire Line
	7300 4200 8550 4200
Wire Wire Line
	7300 4300 8550 4300
Wire Wire Line
	7300 4400 8550 4400
Wire Wire Line
	7300 4500 8550 4500
Wire Wire Line
	7300 4600 8550 4600
Wire Wire Line
	7300 4700 8550 4700
Wire Wire Line
	7300 4800 8550 4800
Wire Wire Line
	7300 4900 8550 4900
Wire Wire Line
	7300 5100 8550 5100
Wire Wire Line
	7300 5200 8550 5200
Wire Wire Line
	7300 5300 8550 5300
Wire Wire Line
	7300 5400 8550 5400
Wire Wire Line
	7300 5500 8550 5500
Wire Wire Line
	2450 4900 3300 4900
Wire Wire Line
	2450 4800 3050 4800
Wire Wire Line
	2450 4700 2650 4700
Wire Wire Line
	2850 4700 3500 4700
Wire Wire Line
	3050 5050 3050 4800
Wire Wire Line
	3050 5700 3050 5350
Wire Wire Line
	2550 5700 3050 5700
Wire Wire Line
	3300 5700 3300 5450
Wire Wire Line
	3175 5800 3175 5700
Connection ~ 3175 5700
Connection ~ 3050 5700
Wire Wire Line
	2450 5100 2550 5100
Wire Wire Line
	2550 5100 2550 5200
Connection ~ 2550 5700
Wire Wire Line
	3175 6000 3175 6050
Wire Wire Line
	3200 4400 3500 4400
Wire Wire Line
	3500 4400 3500 4700
Connection ~ 3500 4700
Connection ~ 6200 2800
Wire Wire Line
	4650 2900 4650 2800
Connection ~ 4900 2800
Wire Wire Line
	4650 3200 4650 3300
Wire Wire Line
	1450 1350 1450 1650
Wire Wire Line
	1550 1350 1550 1650
Wire Wire Line
	1650 1350 1650 1650
Wire Wire Line
	1750 1350 1750 1650
Wire Wire Line
	1850 1350 1850 1650
Wire Wire Line
	1950 1350 1950 1650
Wire Wire Line
	2050 1350 2050 1650
Wire Wire Line
	1550 2550 1550 2850
Wire Wire Line
	1650 2550 1650 2850
Wire Wire Line
	1750 2550 1750 2850
Wire Wire Line
	1850 2550 1850 2850
Wire Wire Line
	1950 2550 1950 2850
Wire Wire Line
	2050 2550 2050 2850
Wire Wire Line
	2150 2550 2150 2850
Wire Wire Line
	2250 2550 2250 2850
Wire Wire Line
	2350 2850 2350 2550
Wire Wire Line
	2150 1350 2150 1650
Wire Wire Line
	2250 1350 2250 1650
Wire Wire Line
	2350 1350 2350 1650
Wire Wire Line
	2450 1350 2450 1650
Wire Wire Line
	2750 2550 2750 2850
Wire Wire Line
	2850 1350 2850 1450
Connection ~ 3725 4700
Wire Wire Line
	2850 1350 3025 1350
Wire Wire Line
	3025 1350 3025 2700
Wire Wire Line
	3025 2700 2850 2700
Wire Wire Line
	2850 2700 2850 2550
Connection ~ 3025 2700
Wire Wire Line
	2550 2850 2550 2550
Wire Wire Line
	4175 3850 4175 3800
Wire Wire Line
	4175 3800 4125 3800
Connection ~ 4125 3800
Wire Wire Line
	4175 4050 4175 4100
Wire Wire Line
	4175 4100 4125 4100
Connection ~ 4125 4100
Wire Wire Line
	2450 5200 2550 5200
Connection ~ 2550 5200
Wire Wire Line
	1350 1550 1350 1350
Text GLabel 4400 4400 0    60   Input ~ 0
UCAP
Wire Wire Line
	4400 4400 4600 4400
Connection ~ 4600 4600
Text GLabel 6950 1200 0    60   Input ~ 0
VCC
Text GLabel 6950 1375 0    60   Input ~ 0
USBVCC
Text GLabel 6950 1550 0    60   Input ~ 0
UCAP
Wire Wire Line
	6950 1200 7375 1200
Wire Wire Line
	7675 1200 7675 1750
Wire Wire Line
	6950 1375 7575 1375
Wire Wire Line
	7575 1375 7575 1750
$Comp
L project:AB2_JMP_02-1 J2
U 1 1 565AC8FA
P 7375 1850
F 0 "J2" V 7525 1800 60  0000 C CNN
F 1 "3V3" V 7375 1600 60  0000 C CNN
F 2 "ab2_jumper:AB2_SJMP_02-1" H 7375 1850 60  0001 C CNN
F 3 "" H 7375 1850 60  0000 C CNN
F 4 "1" V 7375 1850 50  0001 C CNN "DNP"
	1    7375 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 1550 7275 1550
Wire Wire Line
	7275 1550 7275 1700
Wire Wire Line
	7375 1750 7375 1700
Connection ~ 7375 1200
Wire Wire Line
	1450 2850 1450 2550
Wire Wire Line
	7500 2650 7500 3400
Connection ~ 7500 3400
Wire Wire Line
	8400 2650 8400 3500
Connection ~ 8400 3500
Wire Wire Line
	8200 2550 8300 2550
Wire Wire Line
	8300 2550 8300 3600
Connection ~ 8300 3600
NoConn ~ 1350 2550
Wire Wire Line
	4125 4150 3925 4150
Wire Wire Line
	4125 3750 3925 3750
Wire Wire Line
	3600 3950 3600 4150
Wire Wire Line
	6000 5950 6200 5950
Wire Wire Line
	6000 5950 6000 6050
Wire Wire Line
	3175 5700 3300 5700
Wire Wire Line
	3050 5700 3175 5700
Wire Wire Line
	3500 4700 3725 4700
Wire Wire Line
	6200 2800 6200 3000
Wire Wire Line
	4900 2800 6200 2800
Wire Wire Line
	3725 4700 5100 4700
Wire Wire Line
	4125 3800 4125 3850
Wire Wire Line
	4125 4100 4125 4150
Wire Wire Line
	2550 5200 2550 5700
Wire Wire Line
	4600 4600 4600 5500
Wire Wire Line
	7375 1200 7675 1200
Wire Wire Line
	7500 3400 8550 3400
Wire Wire Line
	8400 3500 8550 3500
Wire Wire Line
	8300 3600 8550 3600
$Comp
L graphics:Logo_Open_Hardware_Small #LOGO1
U 1 1 5C6F899F
P 1500 7200
F 0 "#LOGO1" H 1500 7475 50  0001 C CNN
F 1 "Open Source Hardware" H 1500 7494 50  0000 C CNN
F 2 "" H 1500 7200 50  0001 C CNN
F 3 "~" H 1500 7200 50  0001 C CNN
	1    1500 7200
	1    0    0    -1  
$EndComp
$Comp
L project:Crystal_SMD_4pad X1
U 1 1 5CFABD13
P 4125 3950
F 0 "X1" H 4125 3825 50  0000 C CNN
F 1 "16MHz" H 4125 4087 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_0603-4Pin_6.0x3.5mm" H 4125 3950 60  0001 C CNN
F 3 "" H 4125 3950 60  0000 C CNN
F 4 "Fox Electronics" H 4125 3950 50  0001 C CNN "MFN"
F 5 "FC5BQCCMC16.0-T1" H 4125 3950 50  0001 C CNN "MFP"
F 6 "631-1024-1-ND" H 4125 3950 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 4125 3950 50  0001 C CNN "S1PL"
F 8 "FC5BQCCMC16.0-T1" H 4125 3950 50  0001 C CNN "S2PN"
F 9 "Arrow" H 4125 3950 50  0001 C CNN "S2PL"
F 10 "Crystal 16MHz ±30ppm (Tol) ±30ppm (Stability) 20pF FUND 80Ohm 4-Pin SMD T/R" H 4125 3950 50  0001 C CNN "Description"
F 11 "4-SMD (5.00mm x 3.20mm) " H 4125 3950 50  0001 C CNN "Package ID"
	1    4125 3950
	0    1    1    0   
$EndComp
$Comp
L project:AB2_USB USB1
U 1 1 5CFB6CE1
P 2250 4950
F 0 "USB1" H 2100 4550 60  0000 C CNN
F 1 "AB2_USB" H 2265 5381 60  0000 C CNN
F 2 "AB2_USB:AB2_USB_MICRO_SMD" H 2250 4950 60  0001 C CNN
F 3 "" H 2250 4950 60  0000 C CNN
F 4 "Amphenol ICC" H 2250 4950 50  0001 C CNN "MFN"
F 5 "10118193-0001LF" H 2250 4950 50  0001 C CNN "MFP"
F 6 "609-4616-1-ND" H 2250 4950 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 2250 4950 50  0001 C CNN "S1PL"
F 8 "649-10118193-0001LF" H 2250 4950 50  0001 C CNN "S2PN"
F 9 "Mouser" H 2250 4950 50  0001 C CNN "S2PL"
F 10 "USB Connectors 5P MICRO USB TYPE B" H 2250 4950 50  0001 C CNN "Description"
	1    2250 4950
	1    0    0    -1  
$EndComp
$Comp
L device:Varistor VR1
U 1 1 5CFD79DE
P 3050 5200
F 0 "VR1" V 2975 5325 50  0000 L CNN
F 1 "CG0603MLC" V 3150 4975 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2980 5200 50  0001 C CNN
F 3 "~" H 3050 5200 50  0001 C CNN
F 4 "Bourns, Inc" H 3050 5200 50  0001 C CNN "MFN"
F 5 "CG0603MLC-05E" H 3050 5200 50  0001 C CNN "MFP"
F 6 "CG0603MLC-05ECT-ND" H 3050 5200 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 3050 5200 50  0001 C CNN "S1PL"
F 8 "CG0603MLC-05E" H 3050 5200 50  0001 C CNN "S2PN"
F 9 "Arrow" H 3050 5200 50  0001 C CNN "S2PL"
F 10 "ESD Suppressor 5V " H 3050 5200 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric)" H 3050 5200 50  0001 C CNN "Package ID"
	1    3050 5200
	1    0    0    -1  
$EndComp
$Comp
L device:Varistor VR2
U 1 1 5CFED30E
P 3300 5300
F 0 "VR2" V 3225 5425 50  0000 L CNN
F 1 "CG0603MLC" V 3400 5075 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3230 5300 50  0001 C CNN
F 3 "~" H 3300 5300 50  0001 C CNN
F 4 "Bourns, Inc" H 3300 5300 50  0001 C CNN "MFN"
F 5 "CG0603MLC-05E" H 3300 5300 50  0001 C CNN "MFP"
F 6 "CG0603MLC-05ECT-ND" H 3300 5300 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 3300 5300 50  0001 C CNN "S1PL"
F 8 "CG0603MLC-05E" H 3300 5300 50  0001 C CNN "S2PN"
F 9 "Arrow" H 3300 5300 50  0001 C CNN "S2PL"
F 10 "ESD Suppressor 5V " H 3300 5300 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric)" H 3300 5300 50  0001 C CNN "Package ID"
	1    3300 5300
	1    0    0    -1  
$EndComp
$Comp
L device:Polyfuse_Small F1
U 1 1 5CFF831B
P 2750 4700
F 0 "F1" V 2600 4700 50  0000 C CNN
F 1 "MF-MSMF050" V 2675 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_1812_4532Metric" H 2800 4500 50  0001 L CNN
F 3 "~" H 2750 4700 50  0001 C CNN
F 4 "Bourns Inc" H 2750 4700 50  0001 C CNN "MFN"
F 5 "MF-MSMF050-2" H 2750 4700 50  0001 C CNN "MFP"
F 6 "MF-MSMF050-2CT-ND" H 2750 4700 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 2750 4700 50  0001 C CNN "S1PL"
F 8 "MF-MSMF050-2" H 2750 4700 50  0001 C CNN "S2PN"
F 9 "Arrow" H 3300 5300 50  0001 C CNN "S2PL"
F 10 "PTC Resettable Fuse 0.5A(hold) 1A(trip) 15V" H 2750 4700 50  0001 C CNN "Description"
F 11 "1812 (4532 Metric)" H 2750 4700 50  0001 C CNN "Package ID"
	1    2750 4700
	0    1    1    0   
$EndComp
$Comp
L device:C_Small C2
U 1 1 5D0035CD
P 3825 4150
F 0 "C2" V 3900 4000 50  0000 L CNN
F 1 "15p" V 3750 3975 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3825 4150 60  0001 C CNN
F 3 "" H 3825 4150 60  0000 C CNN
F 4 "Kemet" H 3825 4150 50  0001 C CNN "MFN"
F 5 "C0603C150J5GACTU" H 3825 4150 50  0001 C CNN "MFP"
F 6 "399-1051-1-ND" H 3825 4150 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 3825 4150 50  0001 C CNN "S1PL"
F 8 "80-C0603C150J5G" H 3825 4150 50  0001 C CNN "S2PN"
F 9 "Mouser" H 3825 4150 50  0001 C CNN "S2PL"
F 10 "CAP CER 15PF 50V C0G/NP0 0603" H 3825 4150 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric)" H 3825 4150 50  0001 C CNN "Package ID"
	1    3825 4150
	0    1    1    0   
$EndComp
$Comp
L device:R R2
U 1 1 5D014243
P 4100 4900
F 0 "R2" V 4050 5100 50  0000 C CNN
F 1 "22" V 4100 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4030 4900 30  0001 C CNN
F 3 "" H 4100 4900 30  0000 C CNN
F 4 "Panasonic" H 4100 4900 50  0001 C CNN "MFN"
F 5 "ERJ-3EKF22R0V" H 4100 4900 50  0001 C CNN "MFP"
F 6 "P22.0HCT-ND" H 4100 4900 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 4100 4900 50  0001 C CNN "S1PL"
F 8 "667-ERJ-3EKF22R0V" H 4100 4900 50  0001 C CNN "S2PN"
F 9 "Mouser" H 4100 4900 50  0001 C CNN "S2PL"
F 10 "RES SMD 22 OHM 1% 1/10W 0603" H 4100 4900 50  0001 C CNN "Description"
F 11 "0603 (1608 Metric) " H 4100 4900 50  0001 C CNN "Package ID"
	1    4100 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 4900 3300 4900
Connection ~ 3050 4800
Wire Wire Line
	3300 4900 3300 5150
Wire Wire Line
	3050 4800 3950 4800
Connection ~ 3300 4900
Wire Wire Line
	4250 4800 5100 4800
Wire Wire Line
	4250 4900 5100 4900
$Comp
L project:ATMEGA8U2-MU IC1
U 1 1 5E894A25
P 6200 4400
F 0 "IC1" H 5425 5750 60  0000 C CNN
F 1 "ATMEGA32U2" H 6825 5725 50  0000 C CNN
F 2 "ATMega:QFN-32-1EP_5x5mm_Pitch0.5mm" H 6050 4450 50  0001 C CNN
F 3 "" H 6300 2950 50  0001 C CNN
F 4 "Microchip" H 6200 4400 50  0001 C CNN "MFN"
F 5 "ATMEGA32U2-MUR" H 6200 4400 50  0001 C CNN "MFP"
F 6 "ATMEGA32U2-MURCT-ND" H 6200 4400 50  0001 C CNN "S1PN"
F 7 "Digi-Key" H 6200 4400 50  0001 C CNN "S1PL"
F 8 "556-ATMEGA32U2-MUR" H 6200 4400 50  0001 C CNN "S2PN"
F 9 "Mouser" H 6200 4400 50  0001 C CNN "S2PL"
F 10 "MCU 8BIT 32KB FLASH 32VQFN" H 6200 4400 50  0001 C CNN "Description"
F 11 "VQFN-32" H 6200 4400 50  0001 C CNN "Package ID"
	1    6200 4400
	1    0    0    -1  
$EndComp
Text GLabel 6200 2600 1    60   Input ~ 0
VCC
Wire Wire Line
	7275 1700 7375 1700
Connection ~ 7275 1700
Wire Wire Line
	7275 1700 7275 1750
Connection ~ 7375 1700
Wire Wire Line
	7375 1700 7375 1200
$EndSCHEMATC
